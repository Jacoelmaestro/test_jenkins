package org.example.model;

import jakarta.persistence.*;
import jakarta.validation.constraints.Max;
import jakarta.validation.constraints.NotNull;
import lombok.*;

@Entity
@Table(name = "produit")
/*@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode
@ToString*/
@Data
public class Produit {

        @Id
        @GeneratedValue(strategy = GenerationType.IDENTITY)
        @Column(name = "id")
        private  int id;

        @Column(name = "libelle")
        private  String libelle;

        @Max(value = 100)
        @NotNull
        @Column(name = "quantite")
        private  double quantite;

        @Column(name ="prix")
        private  int prix ;


}
