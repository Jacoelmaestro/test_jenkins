package org.example.Repository;

import jakarta.persistence.EntityManager;
import jakarta.persistence.EntityTransaction;
import org.example.model.Produit;

import java.util.List;

public class ProduitRepository
{

        public void insertProduit(Produit produit){

            EntityManager entityManager = JPAUtil.getEntityManagerFactory().createEntityManager();
            EntityTransaction entityTransaction = entityManager.getTransaction();
            entityTransaction.begin();

            //save
            entityManager.persist(produit);
            entityManager.getTransaction().commit();
            entityManager.close();
        }

        public void deleteProduit(int id){

            EntityManager entityManager = JPAUtil.getEntityManagerFactory().createEntityManager();
            entityManager.getTransaction().begin();

            Produit produit = entityManager.find(Produit.class, id);
            entityManager.remove(produit);
            entityManager.getTransaction().commit();
            entityManager.close();

        }

        public Produit findProduitById(int id){

            EntityManager entityManager = JPAUtil.getEntityManagerFactory().createEntityManager();
            entityManager.getTransaction().begin();

            Produit produit = entityManager.find(Produit.class, id);

            entityManager.getTransaction().commit();
            entityManager.close();

            return produit;

        }

        public void updateProduit(int id){
            EntityManager entityManager = JPAUtil.getEntityManagerFactory().createEntityManager();
            entityManager.getTransaction().begin();

            Produit produit= entityManager.find(Produit.class, id);
            // The entity object is physically updated in the database when the transaction
            // is committed
           produit.setLibelle("xxxx");
           produit.setQuantite(0);
           produit.setPrix(0);
            entityManager.getTransaction().commit();
            entityManager.close();

        }

        public List<Produit> getAll(){

            EntityManager entityManager = JPAUtil.getEntityManagerFactory().createEntityManager();
            entityManager.getTransaction().begin();


         /* Produit produit= (Produit) entityManager.createQuery("SELECT t FROM Produit t where t.libelle = :value1")
                    .setParameter("value1", "xxxx").getSingleResult();*/
            List<Produit> produits= (List<Produit>) entityManager.createQuery("SELECT t FROM Produit t").
                    getResultList();

            //getResultList();

            entityManager.getTransaction().commit();
            entityManager.close();

            return  produits;

        }

        public  List<Produit> getProductsByLibelle(String libelle){

            EntityManager entityManager = JPAUtil.getEntityManagerFactory().createEntityManager();
            entityManager.getTransaction().begin();


            List<Produit> produits= (List<Produit>) entityManager.createQuery("SELECT t FROM Produit t where t.libelle = :value1")
                    .setParameter("value1", libelle).getResultList();

            entityManager.getTransaction().commit();
            entityManager.close();

            return  produits;
        }

}
